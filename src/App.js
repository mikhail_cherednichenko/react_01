import NewCost from "./components/NewCost/NewCost";
import Costs from "./components/Costs/Costs";
import { useState } from "react";

const INITIAL_COSTS = [
  {
    id: "c1",
    date: new Date(2021, 2, 12),
    description: "Холодильник",
    amount: 999.99,
  },
  {
    id: "c2",
    date: new Date(2022, 4, 12),
    description: "Телефон",
    amount: 99.99,
  },
  {
    id: "c3",
    date: new Date(2023, 5, 12),
    description: "Інтернет",
    amount: 30,
  },
];

const App = () => {
  const [costs, setCosts] = useState(INITIAL_COSTS);

  //Стара форма творення компоненту react
  // return React.createElement(
  //   "div",
  //   {},
  //   React.createElement("h1", {}, "Hello World"),
  //   React.createElement(costs, { costs: costs })
  // );

  const addCostHandler = (cost) => {
    setCosts((prevCosts) => {
      return [cost, ...prevCosts];
    });
  };

  const changeFilter = (year) => {
    console.log(year);
    console.log("App");
  };

  return (
    <div>
      {/* <h1>Hello world</h1> */}
      <NewCost onAddCost={addCostHandler} />
      <Costs costs={costs} onChangeCostFilter={changeFilter} />
    </div>
  );
};

export default App;

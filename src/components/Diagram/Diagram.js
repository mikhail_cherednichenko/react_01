import "./Diagram.css";
import Card from "../UI/Card";
import DiagramBar from "./DiagramBar";
// import { useState } from "react";

const Diagram = (props) => {
  const dataSetsValues = props.dataSets.map((dataSet) => dataSet.value);

  const maxMonthCosts = Math.max(...dataSetsValues);

  return (
    <Card className="diagram">
      {props.dataSets.map((dataSet) => (
        <DiagramBar
          key={dataSet.id}
          value={dataSet.value}
          maxValue={maxMonthCosts}
          label={dataSet.label}
        />
      ))}
    </Card>
  );
};

export default Diagram;

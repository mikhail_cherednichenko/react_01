import { useState } from "react";
import "./CostForm.css";

const CostForm = (props) => {
  const [inputDescription, setinputDescription] = useState("");
  const [inputAmount, setInputAmount] = useState("");
  const [inputDate, setInputDate] = useState("");

  // const [userInput, setUserInput] = useState({
  //   name: "",
  //   amount: "",
  //   date: "",
  // });

  const descriptionChangeHandler = (event) => {
    setinputDescription(event.target.value);

    // setUserInput({
    //   ...userInput,
    //   name: event.target.value,
    // });

    // setUserInput((previousState) => {
    //   return {
    //     ...previousState,
    //     name: event.target.name,
    //   };
    // });
  };
  const amountChangeHandler = (event) => {
    setInputAmount(event.target.value);

    // setUserInput({
    //   ...userInput,
    //   amount: event.target.value,
    // });
  };
  const dateChangeHandler = (event) => {
    setInputDate(event.target.value);

    // setUserInput({
    //   ...userInput,
    //   date: event.target.value,
    // });
  };

  const submitHandler = (event) => {
    event.preventDefault(); //Скасування перезапуску сторінки, тому що відбувається через вбудований метод submit

    const costData = {
      description: inputDescription,
      amount: inputAmount,
      date: new Date(inputDate),
    };

    // console.log(costData)

    props.onSaveCostData(costData);

    setinputDescription('')
    setInputAmount('')
    setInputDate('')
  };

  return (
    <form onSubmit={submitHandler}>
      <div className="new-cost__controls">
        <div className="new-cost__control">
          <label>Назва</label>
          <input
            type="text"
            value={inputDescription}
            onChange={descriptionChangeHandler}
          />
        </div>
        <div className="new-cost__control">
          <label>Варість</label>
          <input
            type="number"
            min="0.01"
            step="0.01"
            value={inputAmount}
            onChange={amountChangeHandler}
          />
        </div>
        <div className="new-cost__control">
          <label>Дата</label>
          <input
            type="date"
            min="2019-01-01"
            value={inputDate}
            onChange={dateChangeHandler}
          />
        </div>
        <div className="new-cost__actions">
          <button type="submit">Додати</button>
          <button type="button" onClick={props.onStateChange}>Скасувати</button>
        </div>
      </div>
    </form>
  );
};

export default CostForm;

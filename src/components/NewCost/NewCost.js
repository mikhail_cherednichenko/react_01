import { useState } from "react";
import CostForm from "./CostForm";
import "./NewCost.css";

const NewCost = (props) => {
  const [newCostState, setNewCostState] = useState(false)

  const saveCostDataHandler = (inputCostData) => {
    const costData = {
      ...inputCostData,
      id: Math.random().toString(),
    };

    props.onAddCost(costData);
    // console.log(costData);
    // const costData = {
    //   ...inputCostData //Розкладаємо отриманий об'єкт з компонента

    // }
    setNewCostStateHandler()
  };

  const setNewCostStateHandler = () => {
    setNewCostState(!newCostState);
  }

  let content = "";

  if (newCostState) {
    content = (
      <CostForm
        onSaveCostData={saveCostDataHandler}
        onStateChange={setNewCostStateHandler}
      />
    );
  } else {
    content = (
      <button onClick={setNewCostStateHandler}>Додати покупку</button>
    );
  }

  return <div className="new-cost">{content}</div>;
};

export default NewCost;

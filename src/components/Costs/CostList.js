import "./CostList.css";
import CostItem from "./CostItem";

const CostList = (props) => {
  if (props.costs.length === 0) {
    return <p className="cost-list__fallback">Розходи не зафіксовано</p>;
  }

  return (
    <ul className="cost-list">
      {props.costs.map((cost) => (
        <CostItem
          key={cost.id} //Обов'язковий елемент для розмітки реакт
          date={cost.date}
          description={cost.description}
          amount={cost.amount}
        />
      ))}
    </ul>
  );
};

export default CostList;

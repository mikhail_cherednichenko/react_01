import "./Costs.css";
import Card from "../UI/Card";
import CostFilter from "./CostFilter";
import { useState } from "react";
import CostList from "./CostList";
import Diagram from "../Diagram/Diagram";
import CostsDiagram from "./CostsDiagram";

const Costs = (props) => {
  const [selectedYear, setSelectedYear] = useState(new Date().getFullYear());

  const yearChangeHandler = (year) => {
    props.onChangeCostFilter(year);
    // console.log(year)
    setSelectedYear(year);
  };

  const filteredCosts = props.costs.filter((cost) => {
    return cost.date.getFullYear() == selectedYear;
  });

  return (
    <Card className="costs">
      <CostFilter year={selectedYear} onChancgeCostFilter={yearChangeHandler} />
      <CostsDiagram costs={filteredCosts} />
      <CostList costs={filteredCosts} />
      {/* {filteredCosts.length === 0 && <p>Розходи не зафіксовано</p>}
      {filteredCosts.map((cost) => (
        <CostItem
          key={cost.id} //Обов'язковий елемент для розмітки реакт
          date={cost.date}
          description={cost.description}
          amount={cost.amount}
        />
      ))} */}
      {/* <CostItem
        date={props.costs[0].date}
        description={props.costs[0].description}
        amount={props.costs[0].amount}
      />
      <CostItem
        date={props.costs[1].date}
        description={props.costs[1].description}
        amount={props.costs[1].amount}
      />
      <CostItem
        date={props.costs[2].date}
        description={props.costs[2].description}
        amount={props.costs[2].amount}
      /> */}
    </Card>
  );
};

export default Costs;

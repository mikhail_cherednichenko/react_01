import Diagram from "../Diagram/Diagram";

const CostsDiagram = (props) => {
  // const months = []

  // for (let i = 0; i < 12; i++) {
  //     months.add(new Date().setMonth(i).toLocaleString('ua-UA', { month: "short" }))
  // }

  const diagramDataSets = [];

  for (let i = 0; i < 12; i++) {
    const d = new Date(99, i, 1);
    d.setMonth(i);
    // let month = new Intl.DateTimeFormat("ua-UA", { month: "short" }).format(
    //   d.setMonth(i)
    // );
    const month = d.toLocaleDateString("ua-UA", { month: "short" });
    diagramDataSets.push({ label: month, value: 0, id: i });
  }

  for (const cost of props.costs) {
    const costMonth = cost.date.getMonth();
    diagramDataSets[costMonth].value += cost.amount;
  }

  return <Diagram dataSets={diagramDataSets} />;
};

export default CostsDiagram;

import "./CostFilter.css";

// import { useState } from "react";

const CostFilter = (props) => {
  //   const [inputYear, setInputYear] = useState("");

  const yearChangeHandler = (event) => {
    props.onChancgeCostFilter(event.target.value);
    // console.log(inputYear)
    // console.log(event.target.value);
  };

  return (
    <div className="costs-header">
      <label className="costs-header__description">Оберіть рік</label>
      {/* <input type="text" list="years" onChange={yearChangeHandler} /> */}
      <select
        type="text"
        list="years"
        onChange={yearChangeHandler}
        value={props.year}
      >
        <option value="2020">2020</option>
        <option value="2021">2021</option>
        <option value="2022">2022</option>
        <option value="2023">2023</option>
      </select>
    </div>
  );
};

export default CostFilter;
